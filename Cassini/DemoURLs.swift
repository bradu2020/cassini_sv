//
//  DemoURLs.swift
//  Cassini
//
//  Created by CS193p Instructor.
//  Copyright © 2017 Stanford University. All rights reserved.
//

import Foundation

struct DemoURLs
{
    //Local, needs to be in the project, Target membership Ticked to get included when installing on device
	//static let stanford = Bundle.main.url(forResource: "oval", withExtension: "jpg")

    static let stanford = URL(string: "https://images.wallpaperscraft.com/image/paint_stains_mixing_138716_3840x2400.jpg")
	
    static var NASA: Dictionary<String,URL> = {
        let NASAURLStrings = [
            "Cassini" : "https://www.wallpapers13.com/wp-content/uploads/2015/12/Cassini-Probe-near-the-planet-Saturn-rings-of-dust-Desktop-HD-Wallpaper-for-Mobile-phones-Tablet-and-PC-3840x2400.jpg",
            "Earth" : "https://images.hdqwalls.com/download/earth-moon-4k-ri-3840x2400.jpg",
            "Saturn" : "https://i.pinimg.com/originals/78/27/d0/7827d06fd6fa29dbf58f0d8141850444.jpg"
        ]
        var urls = Dictionary<String,URL>()
        for (key, value) in NASAURLStrings {
            urls[key] = URL(string: value)
        }
        return urls
    }()
}
