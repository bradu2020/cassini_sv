//
//  ImageViewController.swift
//  Cassini
//
//  Created by XeVoNx on 13/05/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, UIScrollViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //inform the user #later
        if imageURL == nil {
            imageURL = DemoURLs.stanford
        }
        
    }

    //The model (could be local/internet file url)
    var imageURL: URL? {
        //not computed, the var retains a value
        didSet {
            
            //clear img if any
            image = nil //this comp prop set{} ensures the content size and img size is cleared as well
            
            //only fetch that img if someone sets the model and i'm on screen
            if view.window != nil { //if view is on screen, it will have a window var
                fetchImage()
            }
            
        }
    }
    
    private var image: UIImage? {
        //computed property to get/set img from imageView and do a setting on the side
        get {
            return imageView.image
        }
        set {
            //The following lines also make the sizes 0 when imageURL is set to nil
            imageView.image = newValue
            //change size from 0,0 to intrinsic size (fit IMG best)
            imageView.sizeToFit()
            //and now change 0,0 size of content to the img size
            scrollView?.contentSize = imageView.frame.size
            //Optional chainig - To avoid it crashing when prepare for segue
            
            //stop the spinner only if the img is actually set
            spinner?.stopAnimating()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //if i'm not on screen when imageURL is set,now i am and i'll fetch it
        if imageView.image == nil {
            fetchImage()
            //also if in background, give the user some indication i'm working on it
        }
    }
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    //2) And as soon as scrollView is hooked up by IB, add the imageView created from code
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.minimumZoomScale = 1/25
            scrollView.maximumZoomScale = 1.0
            scrollView.delegate = self
            
            scrollView.addSubview(imageView)
        }
    }
    
    //All SV del methods are optionals but this needs to be implemented
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    //1) instead of outlet from IB
    var imageView = UIImageView()
    
    private func fetchImage() {
        
        if let url = imageURL {
            //turn spinner on before expensive operation
            spinner.startAnimating()
            //If the img is large, this Data() won't return and the app hangs -Fix> dispatch
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                //get data from internet url, using the Data object initializer
                //do { //or convert this into an optional with ?
                    let urlContents = try? Data(contentsOf: url)
                //} catch let error {
                    //log the err
                //}
                
                //Now the problem is this .image 's setter does UI that needs to be on main thread
                DispatchQueue.main.async {
                    //And now we make sure that this img is needed when it comes back
                    //EX: user still on this VC that requested it
                    if let imageData = urlContents, url == self?.imageURL {
                        //No mem cyc, no pointer from self to this {}
                        //But if the VC should not be in the heap (user clicks somewhere else while
                        //the img was loading, its still kept in the heap by this closure
                        //use [weak self] to fix this and optional chain self
                        self?.image = UIImage(data: imageData)
                            
                    }
                }
                
            }
        }
            
    }

}
