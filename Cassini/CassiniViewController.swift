//
//  CassiniViewController.swift
//  Cassini
//
//  Created by XeVoNx on 14/05/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

class CassiniViewController: UIViewController {

    //MARK: - Navigation
    
    //Prepare happens befofre outlets are set -> scrollView is nil -> crash
    //Fix: optional chain image set scrollView?.contentSize =
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if let url = DemoURLs.NASA[identifier] {
               
                if let imageVC = segue.destination.contents as? ImageViewController {
                    //imageURL was the public api (model)
                    imageVC.imageURL = url
                    //not so bad because the title is localized to the local language
                    imageVC.title = (sender as? UIButton)?.currentTitle
                }
            }
        }
    }

}

extension UIViewController {
    //If its a VC its just itself
    //If its a navcon, return its visible VC
    var contents: UIViewController {
        if let navcon = self as? UINavigationController {
            return navcon.visibleViewController ?? self
        } else {
            return self
        } //else if let tabbar = 
        //You can do this with a tabbar, else if tababrcon show VC that's current tab
    }
}
